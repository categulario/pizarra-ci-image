# Gtk build image

Esta imagen está diseñada para construir binarios distribuibles de alguna
aplicación escrita con gtk y rust. En este momento esa aplicación es la pizarra.

## Building

Just this:

    docker build -t pizarra-ci-builds .

and you can try it with

    docker run -it pizarra-ci-builds

## Deploying

Create a tag for the release:

    docker tag pizarra-ci-builds:latest categulario/pizarra-ci-builds:latest

And publish it

    docker push categulario/pizarra-ci-builds:latest
